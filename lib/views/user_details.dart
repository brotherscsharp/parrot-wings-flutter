import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:parrot_wings_flutter/authentication/authentication.dart';

class UserDetailsView extends StatefulWidget {
  const UserDetailsView({super.key, this.isActionNeeded = true});

  final bool isActionNeeded;

  @override
  State<UserDetailsView> createState() => _UserDetailsViewState();
}

class _UserDetailsViewState extends State<UserDetailsView> {
  late AuthenticationBloc _appStateBloc;
  double balance = 0;
  bool isLoading = false;

  @override
  void initState() {
    _appStateBloc = BlocProvider.of<AuthenticationBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthenticationBloc, AuthenticationState>(
      builder: (context, state) {
        balance = state.user.balance ?? balance;
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Container(
                decoration: const BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: SizedBox(
                  height: 100,
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Column(
                          children: [
                            Text(
                              state.user.name,
                              style: const TextStyle(
                                  fontFamily: 'Oswald', fontSize: 25),
                            ),
                            Text(
                              '(${state.user.email})',
                              style: const TextStyle(
                                  fontFamily: 'Oswald',
                                  fontSize: 16,
                                  fontWeight: FontWeight.w200),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Column(
                              children: [
                                const Text(
                                  'Your balanse:',
                                  style: TextStyle(
                                      fontFamily: 'Oswald', fontSize: 25),
                                ),
                                Text(
                                  '\$ ${state.user.balance ?? balance}',
                                  style: const TextStyle(
                                      fontFamily: 'Oswald',
                                      fontSize: 16,
                                      fontWeight: FontWeight.w200),
                                ),
                              ],
                            ),
                          ],
                        ),
                        ElevatedButton(
                          key: const Key('loginForm_continue_raisedButton'),
                          onPressed: () {
                            _appStateBloc.add(SendFundsNavigation());
                          },
                          child: const Text('Send'),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
