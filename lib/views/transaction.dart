import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:parrot_wings_flutter/authentication/authentication.dart';
import 'package:parrot_wings_flutter/transaction/transaction.dart';

class TransactionsView extends StatefulWidget {
  const TransactionsView({super.key});

  @override
  State<TransactionsView> createState() => _TransactionsViewState();
}

class _TransactionsViewState extends State<TransactionsView> {
  late AuthenticationBloc _appStateBloc;
  late final AuthenticationState _state;

  bool isLoading = false;

  @override
  void initState() {
    _appStateBloc = BlocProvider.of<AuthenticationBloc>(context);
    _state = _appStateBloc.state;
    super.initState();
  }

  void navigateOk() {
    Navigator.pop(context, 'OK');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (context, state) {
          return LayoutBuilder(
            builder: (context, constraints) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: ListView.builder(
                  itemCount: _state.transactions.length,
                  itemBuilder: (context, index) {
                    final item = _state.transactions[index];
                    return GestureDetector(
                      onTap: () => item.amount > 0
                          ? null
                          : showDialog<String>(
                              context: context,
                              builder: (BuildContext context) => AlertDialog(
                                title: const Text('Send funds'),
                                content: Text(
                                    'Are you sure you want to send \$${item.amount.abs()} to ${item.username}?'),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () =>
                                        Navigator.pop(context, 'Cancel'),
                                    child: const Text('No'),
                                  ),
                                  TextButton(
                                    onPressed: () => {
                                      context.read<TransactionBloc>().add(
                                            TransactionUsernameChanged(
                                                item.username),
                                          ),
                                      context.read<TransactionBloc>().add(
                                            TransactionAmountChanged(
                                                item.amount.abs()),
                                          ),
                                      context
                                          .read<TransactionBloc>()
                                          .add(const TransactionSubmitted()),
                                      context
                                          .read<AuthenticationBloc>()
                                          .add(UpdateUserInfoRequested()),
                                      Navigator.pop(context, 'OK'),
                                    },
                                    child: const Text('Yes'),
                                  ),
                                ],
                              ),
                            ),
                      child: Container(
                        color: index % 2 == 0
                            ? Colors.grey.shade100
                            : Colors.white,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Column(children: [
                                  if (item.amount > 0)
                                    const Icon(
                                      Icons.arrow_downward,
                                      color: Colors.green,
                                    )
                                  else
                                    const Icon(Icons.arrow_upward,
                                        color: Colors.red),
                                ]),
                              ),
                              Expanded(
                                flex: 5,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Column(children: [
                                      Row(
                                        children: [
                                          item.amount > 0
                                              ? RichText(
                                                  text: TextSpan(
                                                    // Note: Styles for TextSpans must be explicitly defined.
                                                    // Child text spans will inherit styles from parent
                                                    style: const TextStyle(
                                                        color: Colors.black,
                                                        fontFamily: 'Oswald',
                                                        fontSize: 18),
                                                    children: <TextSpan>[
                                                      const TextSpan(
                                                          text:
                                                              'You received money from user '),
                                                      TextSpan(
                                                          text: item.username,
                                                          style: const TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold)),
                                                    ],
                                                  ),
                                                )
                                              : RichText(
                                                  text: TextSpan(
                                                    // Note: Styles for TextSpans must be explicitly defined.
                                                    // Child text spans will inherit styles from parent
                                                    style: const TextStyle(
                                                        color: Colors.black,
                                                        fontFamily: 'Oswald',
                                                        fontSize: 18),
                                                    children: <TextSpan>[
                                                      const TextSpan(
                                                          text: 'User '),
                                                      TextSpan(
                                                          text: item.username,
                                                          style: const TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold)),
                                                      const TextSpan(
                                                          text:
                                                              ' sent you money'),
                                                    ],
                                                  ),
                                                ),
                                        ],
                                      ),
                                      Text(
                                        item.date,
                                        style: const TextStyle(
                                            fontFamily: 'Oswald', fontSize: 10),
                                      ),
                                    ]),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Column(children: [
                                  Text(
                                    '\$${item.amount}',
                                    style: const TextStyle(
                                        fontFamily: 'Oswald', fontSize: 18),
                                  ),
                                  Text(
                                    '\$${item.balance}',
                                    style: const TextStyle(
                                        fontFamily: 'Oswald', fontSize: 10),
                                  ),
                                ]),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              );
            },
          );
        },
      ),
    );
  }
}
