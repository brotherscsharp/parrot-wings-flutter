import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:formz/formz.dart';
import 'package:parrot_wings_flutter/authentication/authentication.dart';
import 'package:parrot_wings_flutter/core/notifications.dart';
import 'package:parrot_wings_flutter/models/requests/user_filter.dart';
import 'package:parrot_wings_flutter/repository/parrot_wings_client.dart';
import 'package:parrot_wings_flutter/transaction/transaction.dart';

class SendFunds extends StatefulWidget {
  const SendFunds({super.key});

  static Route<void> route() {
    return MaterialPageRoute<void>(builder: (_) => const SendFunds());
  }

  @override
  State<SendFunds> createState() => _SendFundsState();
}

class _SendFundsState extends State<SendFunds> {
  final _formKey = GlobalKey<FormState>();
  late TextEditingController name;

  String? selectedUser;
  bool isLoading = false;

  final ParrotWingsClient _apiClient = ParrotWingsClient();

  @override
  void initState() {
    context.read<TransactionBloc>().add(const TransactionRenewChanged());
    name = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    name.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<TransactionBloc, TransactionState>(
      listener: (context, transactionState) {
        if (transactionState.status == FormzSubmissionStatus.failure) {
          Notifications.show(context, transactionState.error,
              color: Colors.red);
        } else if (transactionState.status == FormzSubmissionStatus.success) {
          Notifications.show(context, 'Funds sucessfully sent');
          context.read<AuthenticationBloc>().add(SendFundsNavigationBack());
        }
      },
      child: Scaffold(
        body: LayoutBuilder(
          builder: (context, constraints) {
            return BlocBuilder<AuthenticationBloc, AuthenticationState>(
              builder: (context, state) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Container(
                        decoration: const BoxDecoration(
                            color: Colors.black12,
                            borderRadius:
                                BorderRadius.all(Radius.circular(20))),
                        child: const Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: EdgeInsets.all(20.0),
                                  child: Text(
                                    'Send funds',
                                    style: TextStyle(
                                        fontFamily: 'Oswald', fontSize: 18),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      Form(
                        key: _formKey,
                        child: Center(
                          child: Column(
                            children: [
                              SizedBox(
                                width: constraints.maxWidth < 450
                                    ? constraints.maxWidth
                                    : 300,
                                child: Column(
                                  children: [
                                    Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(5.0),
                                          child: TypeAheadFormField(
                                            textFieldConfiguration:
                                                TextFieldConfiguration(
                                                    controller: name,
                                                    decoration:
                                                        const InputDecoration(
                                                      border:
                                                          OutlineInputBorder(),
                                                      labelText: 'Name',
                                                    )),
                                            suggestionsCallback:
                                                (pattern) async {
                                              return await _apiClient
                                                  .getFilteredUsers(
                                                      UserFilterRequest(pattern)
                                                          .toJson());
                                            },
                                            itemBuilder: (context, suggestion) {
                                              return ListTile(
                                                title: Text(suggestion.name),
                                              );
                                            },
                                            transitionBuilder: (context,
                                                suggestionsBox, controller) {
                                              return suggestionsBox;
                                            },
                                            onSuggestionSelected: (suggestion) {
                                              name.text = suggestion.name;
                                              context
                                                  .read<TransactionBloc>()
                                                  .add(
                                                      TransactionUsernameChanged(
                                                          suggestion.name));
                                            },
                                            validator: (value) {
                                              if (value!.isEmpty) {
                                                return 'Please select a user';
                                              }
                                              return null;
                                            },
                                            onSaved: (value) => context
                                                .read<TransactionBloc>()
                                                .add(TransactionUsernameChanged(
                                                    value.toString())),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(5.0),
                                          child: TextFormField(
                                            onChanged: (amount) => context
                                                .read<TransactionBloc>()
                                                .add(TransactionAmountChanged(
                                                    double.parse(amount))),
                                            decoration: const InputDecoration(
                                              border: OutlineInputBorder(),
                                              labelText: 'Amount',
                                            ),
                                            validator: (value) {
                                              if (value == null ||
                                                  value.isEmpty) {
                                                return 'Please enter some text';
                                              }
                                              return null;
                                            },
                                          ),
                                        ),
                                        Text(
                                          'available balanse: ${state.user.balance ?? 0}',
                                          style: const TextStyle(
                                              fontFamily: 'Oswald',
                                              fontSize: 14,
                                              fontWeight: FontWeight.w200,
                                              fontStyle: FontStyle.italic),
                                        ),
                                      ],
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: ElevatedButton.icon(
                                        icon: isLoading
                                            ? const SizedBox(
                                                height: 20,
                                                width: 20,
                                                child:
                                                    CircularProgressIndicator(
                                                  semanticsLabel: 'loading',
                                                ),
                                              )
                                            : const SizedBox(),
                                        label: const Text('Send'),
                                        style: ElevatedButton.styleFrom(
                                          minimumSize:
                                              const Size.fromHeight(50),
                                        ),
                                        onPressed: isLoading
                                            ? null
                                            : () => context
                                                .read<TransactionBloc>()
                                                .add(
                                                    const TransactionSubmitted()), //sendFunds(),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                          minimumSize:
                                              const Size.fromHeight(50),
                                          foregroundColor: Colors.red,
                                        ),
                                        onPressed: () => {
                                          BlocProvider.of<AuthenticationBloc>(
                                                  context)
                                              .add(SendFundsNavigationBack()),
                                        },
                                        child: const Text("Cancel"),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            );
          },
        ),
      ),
    );
  }
}
