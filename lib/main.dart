import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:parrot_wings_flutter/authentication/bloc/authentication_bloc.dart';
import 'package:parrot_wings_flutter/home/home.dart';
import 'package:parrot_wings_flutter/login/view/login_page.dart';
import 'package:parrot_wings_flutter/registration/view/view.dart';
import 'package:parrot_wings_flutter/repository/app_repository.dart';
import 'package:parrot_wings_flutter/repository/authentication_repository.dart';
import 'package:parrot_wings_flutter/repository/parrot_wings_client.dart';
import 'package:parrot_wings_flutter/repository/transaction_repository.dart';
import 'package:parrot_wings_flutter/repository/user_repository.dart';
import 'package:parrot_wings_flutter/sendFunds/view/send_funds_page.dart';
import 'package:parrot_wings_flutter/splash/view/splash_page.dart';
import 'package:parrot_wings_flutter/transaction/transaction.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});
  final AppRepository repository = AppRepository(ParrotWingsClient());

  final AuthenticationRepository _authenticationRepository =
      AuthenticationRepository(ParrotWingsClient());
  late final UserRepository _userRepository =
      UserRepository(ParrotWingsClient());
  late final TransactionRepository _transactionRepository =
      TransactionRepository(ParrotWingsClient());

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider.value(
      value: _authenticationRepository,
      child: MultiBlocProvider(
        providers: [
          BlocProvider<AuthenticationBloc>(
            create: (context) => AuthenticationBloc(
              authenticationRepository: _authenticationRepository,
              userRepository: _userRepository,
              transactionRepository: _transactionRepository,
            ),
          ),
          BlocProvider<TransactionBloc>(
            create: (context) => TransactionBloc(
              transactionRepository: _transactionRepository,
            ),
          ),
        ],
        child: const SafeArea(child: MyHomePage(title: 'Parrot Wings')),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var selectedIndex = 0;
  final _navigatorKey = GlobalKey<NavigatorState>();
  NavigatorState get _navigator => _navigatorKey.currentState!;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.grey),
        useMaterial3: true,
      ),
      navigatorKey: _navigatorKey,
      builder: (context, child) {
        return BlocListener<AuthenticationBloc, AuthenticationState>(
          listener: (context, state) {
            switch (state.status) {
              case AuthenticationStatus.authenticated:
              case AuthenticationStatus.sendFundsNavigationBack:
                _navigator.pushAndRemoveUntil<void>(
                  HomePage.route(),
                  (route) => false,
                );
              case AuthenticationStatus.sendFundsNavigation:
                _navigator.pushAndRemoveUntil<void>(
                  SendFunds.route(),
                  (route) => false,
                );
              case AuthenticationStatus.unauthenticated:
                _navigator.pushAndRemoveUntil<void>(
                  LoginPage.route(),
                  (route) => false,
                );
              case AuthenticationStatus.navigateToRegistration:
                _navigator.pushAndRemoveUntil<void>(
                  RegistrationPage.route(),
                  (route) => false,
                );
              case AuthenticationStatus.unknown:
              case AuthenticationStatus.updating:
              case AuthenticationStatus.updated:
                break;
            }
          },
          child: child,
        );
      },
      onGenerateRoute: (_) => SplashPage.route(),
    );
  }
}
