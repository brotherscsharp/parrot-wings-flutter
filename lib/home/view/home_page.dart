import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:parrot_wings_flutter/authentication/authentication.dart';
import 'package:parrot_wings_flutter/views/transaction.dart';
import 'package:parrot_wings_flutter/views/user_details.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  static Route<void> route() {
    return MaterialPageRoute<void>(builder: (_) => const HomePage());
  }

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late AuthenticationBloc _appStateBloc;

  @override
  void initState() {
    _appStateBloc = BlocProvider.of<AuthenticationBloc>(context);
    super.initState();
  }

  void logOut() async {
    _appStateBloc.add(AuthenticationLogoutRequested());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 50.0),
            child: IconButton(
              icon: const Icon(Icons.logout),
              tooltip: 'Log out',
              onPressed: () {
                showDialog<String>(
                  context: context,
                  builder: (BuildContext context) => AlertDialog(
                    title: const Text('Send funds'),
                    content: const Text('Are you sure you want to log out?'),
                    actions: <Widget>[
                      TextButton(
                        onPressed: () => Navigator.pop(context, 'Cancel'),
                        child: const Text('No'),
                      ),
                      TextButton(
                        onPressed: () => {
                          logOut(),
                          Navigator.pop(context, 'Cancel'),
                        },
                        child: const Text('Yes'),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ],
      ),
      body: const Center(
        child: HomePageDetails(),
      ),
    );
  }
}

class HomePageDetails extends StatelessWidget {
  const HomePageDetails({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(
      children: [
        UserDetailsView(),
        Expanded(
          child: TransactionsView(),
        ),
      ],
    );
  }
}
