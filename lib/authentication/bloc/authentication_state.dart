part of 'authentication_bloc.dart';

final class AuthenticationState extends Equatable {
  const AuthenticationState._({
    this.status = AuthenticationStatus.unknown,
    this.user = User.empty,
    this.transactions = const [],
  });

  const AuthenticationState({
    this.status = AuthenticationStatus.unknown,
    this.user = User.empty,
    this.transactions = const [],
  });

  const AuthenticationState.unknown() : this._();

  const AuthenticationState.authenticated(
    User user,
    List<Transaction> transactions,
  ) : this._(
            status: AuthenticationStatus.authenticated,
            user: user,
            transactions: transactions);

  const AuthenticationState.updated(
    User user,
    List<Transaction> transactions,
  ) : this._(
            status: AuthenticationStatus.updated,
            user: user,
            transactions: transactions);

  const AuthenticationState.unauthenticated()
      : this._(status: AuthenticationStatus.unauthenticated);

  final AuthenticationStatus status;
  final User user;
  final List<Transaction> transactions;
  AuthenticationState copyWith({
    AuthenticationStatus? status,
    User? user,
    List<Transaction>? transactions,
  }) {
    return AuthenticationState(
      status: status ?? this.status,
      user: user ?? this.user,
      transactions: transactions ?? this.transactions,
    );
  }

  @override
  List<Object> get props => [status, user];
}
