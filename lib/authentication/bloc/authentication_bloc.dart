import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:parrot_wings_flutter/models/transaction.dart';
import 'package:parrot_wings_flutter/models/user.dart';
import 'package:parrot_wings_flutter/repository/authentication_repository.dart';
import 'package:parrot_wings_flutter/repository/transaction_repository.dart';
import 'package:parrot_wings_flutter/repository/user_repository.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  AuthenticationBloc({
    required AuthenticationRepository authenticationRepository,
    required UserRepository userRepository,
    required TransactionRepository transactionRepository,
  })  : _authenticationRepository = authenticationRepository,
        _userRepository = userRepository,
        _transactionRepository = transactionRepository,
        super(const AuthenticationState.unknown()) {
    on<_AuthenticationStatusChanged>(_onAuthenticationStatusChanged);
    on<AuthenticationLogoutRequested>(_onAuthenticationLogoutRequested);
    on<UpdateUserInfoRequested>(_onUpdateUserInfoRequestedRequested);
    on<SendFundsNavigation>(_onSendFundsNavigation);
    on<SendFundsNavigationBack>(_onSendFundsNavigationBack);
    on<NavigateToRegistration>(_onNavigateToRegistration);
    on<NavigateToLogin>(_onNavigateToLogin);

    _authenticationStatusSubscription = _authenticationRepository.status.listen(
      (status) => add(_AuthenticationStatusChanged(status)),
    );
  }
  final TransactionRepository _transactionRepository;
  final UserRepository _userRepository;
  final AuthenticationRepository _authenticationRepository;

  late StreamSubscription<AuthenticationStatus>
      _authenticationStatusSubscription;

  @override
  Future<void> close() {
    _authenticationStatusSubscription.cancel();
    return super.close();
  }

  Future<void> _onAuthenticationStatusChanged(
    _AuthenticationStatusChanged event,
    Emitter<AuthenticationState> emit,
  ) async {
    switch (event.status) {
      case AuthenticationStatus.unauthenticated:
        return emit(const AuthenticationState.unauthenticated());
      case AuthenticationStatus.authenticated:
        final user = await _tryGetUser();
        final transations = await _tryGetUserTransactions();
        return emit(
          user != null
              ? AuthenticationState.authenticated(user, transations)
              : const AuthenticationState.unauthenticated(),
        );
      case AuthenticationStatus.updated:
      case AuthenticationStatus.updating:
      case AuthenticationStatus.sendFundsNavigation:
      case AuthenticationStatus.sendFundsNavigationBack:
      case AuthenticationStatus.navigateToRegistration:
        break;
      case AuthenticationStatus.unknown:
        return emit(const AuthenticationState.unknown());
    }
  }

  void _onAuthenticationLogoutRequested(
    AuthenticationLogoutRequested event,
    Emitter<AuthenticationState> emit,
  ) {
    _userRepository.logOut();
    _authenticationRepository.logOut();
  }

  void _onSendFundsNavigation(
    SendFundsNavigation event,
    Emitter<AuthenticationState> emit,
  ) {
    emit(state.copyWith(status: AuthenticationStatus.sendFundsNavigation));
  }

  void _onSendFundsNavigationBack(
    SendFundsNavigationBack event,
    Emitter<AuthenticationState> emit,
  ) {
    emit(state.copyWith(status: AuthenticationStatus.sendFundsNavigationBack));
  }

  void _onNavigateToRegistration(
    NavigateToRegistration event,
    Emitter<AuthenticationState> emit,
  ) {
    emit(state.copyWith(status: AuthenticationStatus.navigateToRegistration));
  }

  void _onNavigateToLogin(
    NavigateToLogin event,
    Emitter<AuthenticationState> emit,
  ) {
    emit(state.copyWith(status: AuthenticationStatus.unauthenticated));
  }

  Future<void> _onUpdateUserInfoRequestedRequested(
    UpdateUserInfoRequested event,
    Emitter<AuthenticationState> emit,
  ) async {
    emit(state.copyWith(status: AuthenticationStatus.updating));

    final user = await _tryGetUser();
    final transations = await _tryGetUserTransactions();
    return emit(
      user != null
          ? AuthenticationState.updated(user, transations)
          : const AuthenticationState.unauthenticated(),
    );
  }

  Future<User?> _tryGetUser() async {
    try {
      final user = await _userRepository.getUser();
      return user;
    } catch (_) {
      return null;
    }
  }

  Future<List<Transaction>> _tryGetUserTransactions() async {
    try {
      final user = await _transactionRepository.getTransactions();
      return user;
    } catch (_) {
      return [];
    }
  }
}
