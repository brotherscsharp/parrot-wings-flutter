part of 'authentication_bloc.dart';

sealed class AuthenticationEvent {
  const AuthenticationEvent();
}

final class _AuthenticationStatusChanged extends AuthenticationEvent {
  const _AuthenticationStatusChanged(this.status);

  final AuthenticationStatus status;
}

final class AuthenticationLogoutRequested extends AuthenticationEvent {}

final class UpdateUserInfoRequested extends AuthenticationEvent {}

final class SendFundsNavigation extends AuthenticationEvent {}

final class SendFundsNavigationBack extends AuthenticationEvent {}

final class NavigateToRegistration extends AuthenticationEvent {}

final class NavigateToLogin extends AuthenticationEvent {}
