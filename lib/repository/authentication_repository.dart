import 'dart:async';

import 'package:parrot_wings_flutter/models/requests/login.dart';
import 'package:parrot_wings_flutter/models/requests/registration.dart';
import 'package:parrot_wings_flutter/repository/parrot_wings_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum AuthenticationStatus {
  unknown,
  authenticated,
  unauthenticated,
  sendFundsNavigation,
  sendFundsNavigationBack,
  navigateToRegistration,
  updating,
  updated
}

class AuthenticationRepository {
  final _controller = StreamController<AuthenticationStatus>();
  final ParrotWingsClient client;
  AuthenticationRepository(this.client);

  Stream<AuthenticationStatus> get status async* {
    await Future<void>.delayed(const Duration(seconds: 1));
    yield AuthenticationStatus.unauthenticated;
    yield* _controller.stream;
  }

  Future<void> logIn({
    required String username,
    required String password,
  }) async {
    try {
      var res = await client.login(LoginRequest(password, username).toJson());
      Type type = res.runtimeType;

      if (type == String) {
        return;
      }

      if (res['id_token'] != null) {
        var token = res['id_token'];
        final prefs = await SharedPreferences.getInstance();
        await prefs.setString('token', token);
        _controller.add(AuthenticationStatus.authenticated);
        return token;
      }
    } catch (e) {
      _controller.add(AuthenticationStatus.unauthenticated);
      rethrow;
    }
  }

  Future<void> register({
    required String username,
    required String email,
    required String password,
  }) async {
    try {
      var res = await client.registerUser(
          RegistrationRequest(username, password, email).toJson());
      Type type = res.runtimeType;

      if (type == String) {
        return;
      }

      if (res['id_token'] != null) {
        var token = res['id_token'];
        final prefs = await SharedPreferences.getInstance();
        await prefs.setString('token', token);
        _controller.add(AuthenticationStatus.authenticated);
        return token;
      }
    } catch (e) {
      _controller.add(AuthenticationStatus.unauthenticated);
      rethrow;
    }
  }

  Future<void> logOut() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', '');
    _controller.add(AuthenticationStatus.unauthenticated);
  }

  void dispose() => _controller.close();
}
