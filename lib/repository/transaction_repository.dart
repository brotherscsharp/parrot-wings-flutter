import 'dart:async';

import 'package:parrot_wings_flutter/models/requests/transaction_add.dart';
import 'package:parrot_wings_flutter/models/transaction.dart';
import 'package:parrot_wings_flutter/repository/authentication_repository.dart';
import 'package:parrot_wings_flutter/repository/parrot_wings_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum TransactionStatus {
  undefined,
  error,
  sent,
}

class TransactionRepository {
  final ParrotWingsClient client;
  TransactionRepository(this.client);
  final _controller = StreamController<AuthenticationStatus>();

  Stream<AuthenticationStatus> get status async* {
    await Future<void>.delayed(const Duration(seconds: 1));
    yield AuthenticationStatus.unauthenticated;
    yield* _controller.stream;
  }

  Future<List<Transaction>> getTransactions() async {
    try {
      var result = await client.getTransactions();
      if (result == null) {
        final prefs = await SharedPreferences.getInstance();
        await prefs.setString('token', '');
        return [];
      }
      return result;
    } catch (e) {
      return [];
    }
  }

  Future<dynamic> sendFunds({
    required String username,
    required double amount,
  }) async {
    try {
      var result = await client
          .sendFunds(TransactionAddRequest(username, amount.abs()).toJson());
      if (result == null) {
        final prefs = await SharedPreferences.getInstance();
        await prefs.setString('token', '');
        _controller.add(AuthenticationStatus.unauthenticated);
      }
      _controller.add(AuthenticationStatus.authenticated);
      return result;
    } catch (e) {
      // _controller.add(AuthenticationStatus.unauthenticated);
      rethrow;
    }
  }

  void dispose() => _controller.close();
}
