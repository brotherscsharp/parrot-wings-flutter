import 'package:parrot_wings_flutter/models/transaction.dart';
import 'package:parrot_wings_flutter/models/user.dart';
import 'package:parrot_wings_flutter/repository/parrot_wings_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppRepository {
  final ParrotWingsClient client;
  AppRepository(this.client);

  Future<String?> login(Map<String, dynamic>? data) async {
    try {
      var res = await client.login(data);
      Type type = res.runtimeType;

      if (type == String) {
        return null;
      }

      if (res['id_token'] != null) {
        var token = res['id_token'];
        final prefs = await SharedPreferences.getInstance();
        await prefs.setString('token', token);

        return token;
      }
    } catch (e) {
      rethrow;
    }
    return null;
  }

  Future<String?> registerUser(Map<String, dynamic>? data) async {
    try {
      var res = await client.registerUser(data);
      Type type = res.runtimeType;
      if (type == String) {
        return null;
      }

      if (res['id_token'] != null) {
        var token = res['id_token'];
        final prefs = await SharedPreferences.getInstance();
        await prefs.setString('token', token);
        return token;
      }
    } catch (e) {
      rethrow;
    }
    return null;
  }

  Future<List<Transaction>> getTransactions() async {
    var res = await client.getTransactions();
    return res;
  }

  Future<List<Transaction>> sendFunds(Map<String, dynamic>? data) async {
    try {
      var res = await client.sendFunds(data);
      if (res['trans_token'] != null) {
        var transaction = res['trans_token'];

        return [Transaction.fromJson(transaction)];
      }
      return [];
    } catch (e) {
      rethrow;
    }
  }

  Future<User?> getUser() async {
    try {
      var result = await client.getUser();
      if (result == null) {
        final prefs = await SharedPreferences.getInstance();
        await prefs.setString('token', '');
        return null;
      }
      return result;
    } catch (e) {
      rethrow;
    }
  }

  Future<List<User>> getFilteredUsers(Map<String, dynamic>? data) async {
    try {
      var res = await client.getFilteredUsers(data);
      Type type = res.runtimeType;

      if (type == String) {
        return [];
      }
      return res;
    } catch (e) {
      rethrow;
    }
  }
}
