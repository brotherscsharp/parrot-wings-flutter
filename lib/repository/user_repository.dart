import 'dart:async';

import 'package:parrot_wings_flutter/models/user.dart';
import 'package:parrot_wings_flutter/repository/parrot_wings_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserRepository {
  User? _user;
  final ParrotWingsClient client;
  UserRepository(this.client);

  void logOut() {
    _user = null;
  }

  Future<User?> getUser() async {
    if (_user != null) return _user;

    try {
      var result = await client.getUser();
      if (result == null) {
        final prefs = await SharedPreferences.getInstance();
        await prefs.setString('token', '');
        return null;
      }
      return result;
    } catch (e) {
      return null;
    }
  }
}
