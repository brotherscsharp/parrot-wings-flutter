import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:parrot_wings_flutter/config.dart';
import 'package:parrot_wings_flutter/models/transaction.dart';
import 'package:parrot_wings_flutter/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ParrotWingsClient {
  final Dio httpClient;

  ParrotWingsClient({httpClient})
      : this.httpClient = httpClient ??
            Dio(BaseOptions(
              baseUrl: AppConfig.baseUrl,
              contentType: 'application/json',
            ));

  Future<dynamic> login(Map<String, dynamic>? data) async {
    try {
      Response response = await httpClient.post(
        '/sessions/create',
        data: data,
      );
      return response.data;
    } catch (e) {
      rethrow;
    }
  }

  Future<dynamic> registerUser(Map<String, dynamic>? data) async {
    try {
      Response response = await httpClient.post(
        '/users',
        data: data,
      );
      return response.data;
    } catch (e) {
      rethrow;
    }
  }

  Future<dynamic> getTransactions() async {
    try {
      final prefs = await SharedPreferences.getInstance();
      final token = prefs.getString('token') ?? '';

      Response response = await httpClient.get(
        '/api/protected/transactions',
        options: Options(
          headers: {
            "authorization": 'Bearer $token',
          },
        ),
      );

      if (response.data['trans_token'] != null) {
        var transactions = response.data['trans_token'];
        return List.generate(transactions.length, (i) {
          return Transaction(
              transactions[i]['id'],
              transactions[i]['date'],
              transactions[i]['username'],
              double.parse(transactions[i]['amount'].toString()),
              double.parse(transactions[i]['balance'].toString()));
        });
      }
      return [];
    } catch (e) {
      rethrow;
    }
  }

  Future<dynamic> sendFunds(Map<String, dynamic>? data) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      final token = prefs.getString('token') ?? '';

      Response response = await httpClient.post(
        '/api/protected/transactions',
        data: data,
        options: Options(
          headers: {
            "authorization": 'Bearer $token',
          },
        ),
      );
      return response.data;
    } catch (e) {
      rethrow;
    }
  }

  Future<User?> getUser() async {
    try {
      final prefs = await SharedPreferences.getInstance();
      final token = prefs.getString('token') ?? '';
      Response response = await httpClient.get(
        '/api/protected/user-info',
        options: Options(
          headers: {
            "authorization": 'Bearer $token',
          },
        ),
      );

      if (response.data['user_info_token'] != null) {
        var userInfo = response.data['user_info_token'];
        return User.fromJson(userInfo);
      }

      return null;
    } catch (e) {
      rethrow;
    }
  }

  Future<List<User>> getFilteredUsers(Map<String, dynamic>? data) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      final token = prefs.getString('token') ?? '';

      Response response = await httpClient.post(
        '/api/protected/users/list',
        data: data,
        options: Options(
          headers: {
            "authorization": 'Bearer $token',
          },
        ),
      );
      if (response.data != null) {
        var users = response.data;
        return List.generate(users.length, (i) {
          return User(
            users[i]['id'],
            users[i]['name'],
            null,
            null,
          );
        });
      }

      return [];
    } catch (e) {
      return [];
    }
  }
}
