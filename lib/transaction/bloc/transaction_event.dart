part of 'transaction_bloc.dart';

sealed class TransactionEvent extends Equatable {
  const TransactionEvent();

  @override
  List<Object> get props => [];
}

final class TransactionUsernameChanged extends TransactionEvent {
  const TransactionUsernameChanged(this.username);

  final String username;

  @override
  List<Object> get props => [username];
}

final class TransactionAmountChanged extends TransactionEvent {
  const TransactionAmountChanged(this.amount);

  final double amount;

  @override
  List<Object> get props => [amount];
}

final class TransactionSubmitted extends TransactionEvent {
  const TransactionSubmitted();
}

final class TransactionStatusChanged extends TransactionEvent {
  const TransactionStatusChanged(this.status);

  final TransactionStatus status;
}

final class TransactionRenewChanged extends TransactionEvent {
  const TransactionRenewChanged();
}
