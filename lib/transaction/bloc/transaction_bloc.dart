import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:parrot_wings_flutter/registration/models/models.dart';
import 'package:parrot_wings_flutter/repository/transaction_repository.dart';
import 'package:parrot_wings_flutter/transaction/models/models.dart';

part 'transaction_event.dart';
part 'transaction_state.dart';

class TransactionBloc extends Bloc<TransactionEvent, TransactionState> {
  TransactionBloc({
    required TransactionRepository transactionRepository,
  })  : _transactionRepository = transactionRepository,
        super(const TransactionState()) {
    on<TransactionRenewChanged>(_onRenew);
    on<TransactionUsernameChanged>(_onUsernameChanged);
    on<TransactionAmountChanged>(_onAmountChanged);
    on<TransactionSubmitted>(_onSubmitted);
  }

  final TransactionRepository _transactionRepository;

  void _onUsernameChanged(
    TransactionUsernameChanged event,
    Emitter<TransactionState> emit,
  ) {
    final username = Username.dirty(event.username);
    emit(
      state.copyWith(
        username: username,
        isValid: Formz.validate([state.amount, username]),
      ),
    );
  }

  void _onRenew(
    TransactionRenewChanged event,
    Emitter<TransactionState> emit,
  ) {
    emit(const TransactionState());
  }

  void _onAmountChanged(
    TransactionAmountChanged event,
    Emitter<TransactionState> emit,
  ) {
    final amount = Amount.dirty(event.amount);
    emit(
      state.copyWith(
        amount: amount,
        isValid: Formz.validate([amount, state.username]),
      ),
    );
  }

  Future<void> _onSubmitted(
    TransactionSubmitted event,
    Emitter<TransactionState> emit,
  ) async {
    if (state.isValid) {
      emit(state.copyWith(status: FormzSubmissionStatus.inProgress, error: ''));
      try {
        await _transactionRepository.sendFunds(
          username: state.username.value,
          amount: state.amount.value,
        );
        emit(state.copyWith(status: FormzSubmissionStatus.success));
      } on DioException catch (e) {
        var val = state.copyWith(
            status: FormzSubmissionStatus.failure,
            error: e.response?.data?.toString());
        emit(val);
      }
    }
  }
}
