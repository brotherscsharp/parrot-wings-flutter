part of 'transaction_bloc.dart';

final class TransactionState extends Equatable {
  const TransactionState({
    this.status = FormzSubmissionStatus.initial,
    this.username = const Username.pure(),
    this.amount = const Amount.pure(),
    this.isValid = false,
    this.error = '',
  });

  final FormzSubmissionStatus status;
  final String error;
  final Username username;
  final Amount amount;
  final bool isValid;

  TransactionState copyWith({
    FormzSubmissionStatus? status,
    Username? username,
    Amount? amount,
    bool? isValid,
    String? error,
  }) {
    return TransactionState(
      status: status ?? this.status,
      username: username ?? this.username,
      amount: amount ?? this.amount,
      isValid: isValid ?? this.isValid,
      error: error ?? this.error,
    );
  }

  @override
  List<Object> get props => [status, username, amount, error];
}
