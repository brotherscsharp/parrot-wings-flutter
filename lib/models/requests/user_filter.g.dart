// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_filter.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserFilterRequest _$UserFilterRequestFromJson(Map<String, dynamic> json) =>
    UserFilterRequest(
      json['filter'] as String,
    );

Map<String, dynamic> _$UserFilterRequestToJson(UserFilterRequest instance) =>
    <String, dynamic>{
      'filter': instance.filter,
    };
