import 'package:json_annotation/json_annotation.dart';
part 'registration.g.dart';

@JsonSerializable()
class RegistrationRequest {
  String username;
  String password;
  String email;

  RegistrationRequest(this.username, this.password, this.email);

  factory RegistrationRequest.fromJson(Map<String, dynamic> json) =>
      _$RegistrationRequestFromJson(json);
  Map<String, dynamic> toJson() => _$RegistrationRequestToJson(this);
}
