import 'package:json_annotation/json_annotation.dart';
part 'user_filter.g.dart';

@JsonSerializable()
class UserFilterRequest {
  String filter;

  UserFilterRequest(this.filter);

  factory UserFilterRequest.fromJson(Map<String, dynamic> json) =>
      _$UserFilterRequestFromJson(json);
  Map<String, dynamic> toJson() => _$UserFilterRequestToJson(this);
}
