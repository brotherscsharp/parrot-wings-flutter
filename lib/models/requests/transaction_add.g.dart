// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction_add.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransactionAddRequest _$TransactionAddRequestFromJson(
        Map<String, dynamic> json) =>
    TransactionAddRequest(
      json['name'] as String,
      (json['amount'] as num).toDouble(),
    );

Map<String, dynamic> _$TransactionAddRequestToJson(
        TransactionAddRequest instance) =>
    <String, dynamic>{
      'name': instance.name,
      'amount': instance.amount,
    };
