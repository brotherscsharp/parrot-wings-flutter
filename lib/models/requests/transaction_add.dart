import 'package:json_annotation/json_annotation.dart';
part 'transaction_add.g.dart';

@JsonSerializable()
class TransactionAddRequest {
  String name;
  double amount;

  TransactionAddRequest(this.name, this.amount);

  factory TransactionAddRequest.fromJson(Map<String, dynamic> json) =>
      _$TransactionAddRequestFromJson(json);
  Map<String, dynamic> toJson() => _$TransactionAddRequestToJson(this);
}
