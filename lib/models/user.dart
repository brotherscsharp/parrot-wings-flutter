import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
part 'user.g.dart';

// @JsonSerializable()
// class User {
//   int id;
//   String name;
//   String? email;
//   double? balance;

//   const User(this.id, this.name, this.email, this.balance);

//   factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
//   Map<String, dynamic> toJson() => _$UserToJson(this);

//   static const empty = User(-1, '', null, null);
// }

@JsonSerializable()
class User extends Equatable {
  final int id;
  final String name;
  final String? email;
  final double? balance;
  const User(this.id, this.name, this.email, this.balance);

  @override
  List<Object> get props => [id];

  static const empty = User(-1, '', null, null);

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}
