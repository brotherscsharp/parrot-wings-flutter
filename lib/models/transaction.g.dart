// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Transaction _$TransactionFromJson(Map<String, dynamic> json) => Transaction(
      json['id'] as int,
      json['date'] as String,
      json['username'] as String,
      (json['amount'] as num).toDouble(),
      (json['balance'] as num).toDouble(),
    );

Map<String, dynamic> _$TransactionToJson(Transaction instance) =>
    <String, dynamic>{
      'id': instance.id,
      'date': instance.date,
      'username': instance.username,
      'amount': instance.amount,
      'balance': instance.balance,
    };
