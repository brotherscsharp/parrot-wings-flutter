import 'package:flutter/material.dart';

class Notifications {
  static void show(BuildContext context, String message,
      {Color color = Colors.green}) {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      backgroundColor: color,
    ));
  }
}
