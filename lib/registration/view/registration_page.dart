import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:parrot_wings_flutter/registration/bloc/registration_bloc.dart';
import 'package:parrot_wings_flutter/registration/view/registration_form.dart';
import 'package:parrot_wings_flutter/repository/authentication_repository.dart';

class RegistrationPage extends StatelessWidget {
  const RegistrationPage({super.key});

  static Route<void> route() {
    return MaterialPageRoute<void>(builder: (_) => const RegistrationPage());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Registration')),
      body: Padding(
        padding: const EdgeInsets.all(12),
        child: BlocProvider(
          create: (context) {
            return RegistrationBloc(
              authenticationRepository:
                  RepositoryProvider.of<AuthenticationRepository>(context),
            );
          },
          child: const RegistrationForm(),
        ),
      ),
    );
  }
}
