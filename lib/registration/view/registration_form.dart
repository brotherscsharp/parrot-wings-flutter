import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:parrot_wings_flutter/authentication/authentication.dart';
import 'package:parrot_wings_flutter/core/notifications.dart';
import 'package:parrot_wings_flutter/registration/bloc/registration_bloc.dart';

class RegistrationForm extends StatelessWidget {
  const RegistrationForm({super.key});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return BlocListener<RegistrationBloc, RegistrationState>(
          listener: (context, state) {
            if (state.status.isFailure) {
              Notifications.show(context, 'Authentication Failure',
                  color: Colors.red);
            }
          },
          child: Align(
            alignment: const Alignment(0, -1 / 3),
            child: SizedBox(
              width: constraints.maxWidth < 450 ? constraints.maxWidth : 300,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Image.asset(
                    'images/pw.png',
                    width: 100,
                    height: 100,
                  ),
                  _UsernameInput(),
                  const Padding(padding: EdgeInsets.all(12)),
                  _EmailInput(),
                  const Padding(padding: EdgeInsets.all(12)),
                  _PasswordInput(),
                  const Padding(padding: EdgeInsets.all(12)),
                  _LoginButton(),
                  const Padding(padding: EdgeInsets.all(12)),
                  TextButton(
                    onPressed: () {
                      context.read<AuthenticationBloc>().add(NavigateToLogin());
                    },
                    child: const Text('Have an account?'),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

class _UsernameInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegistrationBloc, RegistrationState>(
      buildWhen: (previous, current) => previous.username != current.username,
      builder: (context, state) {
        return TextField(
          key: const Key('registrationForm_usernameInput_textField'),
          onChanged: (username) => context
              .read<RegistrationBloc>()
              .add(RegistrationUsernameChanged(username)),
          decoration: InputDecoration(
            labelText: 'username',
            errorText:
                state.username.displayError != null ? 'invalid username' : null,
          ),
        );
      },
    );
  }
}

class _EmailInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegistrationBloc, RegistrationState>(
      buildWhen: (previous, current) => previous.username != current.username,
      builder: (context, state) {
        return TextFormField(
          key: const Key('registrationForm_emailInput_textField'),
          validator: (value) => EmailValidator.validate(value!)
              ? null
              : "Please enter a valid email",
          onChanged: (username) => context
              .read<RegistrationBloc>()
              .add(RegistrationEmailChanged(username)),
          decoration: InputDecoration(
            labelText: 'e-mail',
            errorText:
                state.username.displayError != null ? 'invalid email' : null,
          ),
        );
      },
    );
  }
}

class _PasswordInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegistrationBloc, RegistrationState>(
      buildWhen: (previous, current) => previous.password != current.password,
      builder: (context, state) {
        return TextField(
          key: const Key('registrationForm_passwordInput_textField'),
          onChanged: (password) => context
              .read<RegistrationBloc>()
              .add(RegistrationPasswordChanged(password)),
          obscureText: true,
          decoration: InputDecoration(
            labelText: 'password',
            errorText:
                state.password.displayError != null ? 'invalid password' : null,
          ),
        );
      },
    );
  }
}

class _LoginButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegistrationBloc, RegistrationState>(
      builder: (context, state) {
        return state.status.isInProgress
            ? const CircularProgressIndicator()
            : ElevatedButton(
                key: const Key('registrationForm_continue_raisedButton'),
                onPressed: state.isValid
                    ? () {
                        context
                            .read<RegistrationBloc>()
                            .add(const RegistrationSubmitted());
                      }
                    : null,
                child: const Text('Register'),
              );
      },
    );
  }
}
